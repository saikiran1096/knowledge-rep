%This file contains information about enery in both cells and in the real world
%Able to reason about
%	What types of energy are potential or kinetic:
%		?-potential_energy(X). X is a form of potential energy
%		?-kinetic_energy(X). X is a form of kinetic_energy

%	If a system is an isolated or open_system:
%		?-open_system(X). X is an open system
%		?-isolated_system(X). X is an isolated system

%	If a chemical process is spontaneous or not
%		spontaneous_chemical_process(X). X is a spontaneous chemical proceses.

%	Able to calculate the change in free energy (G). See rules for necesary input.
%		?-change_in_free_energy(G). G is the change in free energy.

%	If a reaction is endergonic or exergonic
%		?-endergonic_reaction(X). X is an endergonic reaction.
%		?-exergonic_reaction(X). X is an exergonic reaction.


%types of potential and kinetic energy
potential_energy(potential_energy).
potential_energy(chemical_energy).
potential_energy(gravitational_potential_energy).
potential_energy(electical_potential_energy).

kinetic_energy(kinetic_energy).
kinetic_energy(heat_energy).

%potential and kinetic energy are forms of energy
energy(X) :- potential_energy(X).
energy(X) :- kinetic_energy(X).


%open systems are those that can exchange energy with the surroundings and isolated systems are those that cannot
open_system(X) :- able_to_exchange_with_surroundings(X).
isolated_system(X) :- unable_to_exchange_with_surroundings(X).
%an organism is an example of an open system
open_system(organism).

%spontaneous proceses are proceses that require no input of energy and occur naturally
spontaneous_chemical_process(X) :- process(X), no_input_of_energy(X).
%rust is an example of a spontaneous chemical_process
spontaneous_chemical_process(rusting).

%according to the 2nd law of thermodynamics, if a chemical_process invovles energy transfers, then some energy is lost as heat
energy_is_lost_as_heat_during(X) :- transfer_energy_during(X).
%any spontaneous chemical_process increases the entropy of the system becasuse energy is being lost to the system naturally as heat
increases_entropy(X) :- spontaneous_chemical_process(X).


%the change in free energy is equal o the change in enthalpy - the temperature times the change in system entropy (G = deltaH - TdeltaS)
change_in_free_energy(G) :- change_in_system_enthalpy(H), temperature_in_kelvin(T), change_in_system_entropy(S), G is H - T * S.
%delta G is also the final_free_energy minus the intial free energy
change_in_free_energy(G) :- inital_free_energy(G_init), final_free_energy(G_final), G is G_final - G_init.
%if the change in free energy is negative, then the chemical_process is spontaneous.
spontaneous_chemical_process(X) :- change_in_free_energy(G), G < 0.
spontaneous_chemical_process(X) :- becomes_more_stable(X).

%unstable systems have more free energy while stable ones have less
unstable(X) :- high_free_energy(X).
stable(X) :- low_free_energy(X).

%exergonic reactions involve reactions that lose energy to the enviornment/system. They can be identified by either a net release of energy, or a spontaneous chemical_process
exergonic_reaction(X) :- net_release_of_energy(X).
exergonic_reaction(X) :- change_in_free_energy(G), G < 0.
exergonic_reaction(X) :- spontaneous_chemical_process(X).

%this relation is also an iff relation
net_release_of_energy(X) :- exergonic_reaction(X).
spontaneous_chemical_process(X) :- exergonic_reaction(X).

%endergonic reactions gain free energy from the system. They invovle a net incrase in energy, and a gain in G, and they are not spontaneous chemical_processes
endergonic_reaction(X) :- net_increase_of_energy(X).
endergonic_reaction(X) :- change_in_free_energy(G), G >= 0.
endergonic_reaction(X) :- not_spontaneous_chemical_process(X).

net_increase_of_energy(X) :- endergonic_reaction(X).
not_spontaneous_chemical_process(X) :- endergonic_reaction(X).

%a cell at metabolic equilibrium is dead, and a cell that is not is living
dead_cell(X) :- cell(X), at_metabolic_equilibrium(X).
living_cell(X) :- cell(X), not at_metabolic_equilibrium(X).

%cells generally perform three kinds of work
performs_work(cell,chemical).
performs_work(cell,mechanical).
performs_work(cell,transport).

% reaction X and reaction Y are coupling if X is exergonic, Y is endergonic, and X follows Y.
coupling(X,Y) :- exergonic(X), endergonic(Y), follows(X,Y).

definition(first_law_of_thermodynamics,'energy cannot be created or destroyed, only transfered').
cannot_be_destroyed(X) :- potential_energy(X).
cannot_be_destroyed(X) :- kinetic_energy(X).
cannot_be_created(X) :- potential_energy(X).
cannot_be_created(X) :- kinetic_energy(X).
