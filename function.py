import re

fil = open('cell_components.lp').read()
funcs = re.findall('function\((.+)\)\.',fil)
verbs = set()
for f in funcs:
    v = re.findall('([a-zA-Z_]+)\([a-zA-Z_]+,\[[a-zA-Z,_\(\)]*\]\)',f)
    v = set(v)
    verbs |= v
verbs = sorted(verbs)
for verb in verbs:
    print(
    
        'clear({0}(X,_),{0}(X,_)).\n'\
        'subfunc({0}(X,Y1),{0}(X,Y2)):-\n'\
        '      sublist(Y1,Y2).'
        .format(verb)
    )

